#!/bin/bash
#hu8jmn3
echo ""
echo ""
echo "GIT_BRANCH => ${GIT_BRANCH}"
echo ""
echo "GIT_LOCAL_BRANCH => ${GIT_LOCAL_BRANCH}"

echo "BUILD_NUMBER => ${BUILD_NUMBER}"
echo "BUILD_ID => ${BUILD_ID}"
echo "BUILD_DISPLAY_NAME => ${BUILD_DISPLAY_NAME}"

echo "${GIT_BRANCH}" > tmp.sh
sed -i "s/origin\///g" tmp.sh
rm -rf script_out
mkdir script_out
rm -rf log_sql
mkdir log_sql
NOME=$(cat tmp.sh)
#echo ${NOME}

dbname=db_tcti_sisicmbio
servidor=dbtcti01.icmbio.gov.br

nome_projeto=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}'  | sed -e "s/.git//g" )

if []; then
echo "***************** ERRO ***********"
echo "FAVOR INFORMAFAR O PARAMETRO PDCI_URL_GITLAB_PROJETO"
exit 1
fi

PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';"

vartest=$(PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';")

#echo "vartest=> ${vartest}"

if [ $vartest -eq 0 ]; then

  echo ""
  echo "**************************************"
  echo "  Creando estrutura do jenkins no bd "
  echo "**************************************"
  echo ""
  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "CREATE SCHEMA IF NOT EXISTS jenkins AUTHORIZATION ${username_db_jenkins};"
  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "COMMENT ON SCHEMA jenkins IS 'Tabela responsavel em controlar as atualizações de sql no banco de dados do ambiente';"
  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "CREATE TABLE jenkins.controle_versao (  des_tag text NOT NULL,  script_rodado text NOT NULL,  data_inclusao text NOT NULL DEFAULT now(),  ordem serial NOT NULL,  sistema text NOTNULL, -- tabela com o nome do projeto  CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, script_rodado)) WITH (  OIDS=FALSE ); GRANT ALL ON TABLE jenkins.controle_versao TO usr_jenkins; COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado'; COMMENT ON COLUMN jenkins.controle_versao.sistema IS 'tabela com o nome do projeto'; CREATE INDEX controle_versao_pk_i  ON jenkins.controle_versao  USING btree  (sistema COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"

  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "ALTER TABLE jenkins.controle_versao OWNER TO ${username_db_jenkins};"

  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "ALTER ROLE ${username_db_jenkins} SET search_path TO jenkins;"

fi

for entry in sql/*
do

  if [ -f "$entry" ]; then

    vartest=$(PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM jenkins.controle_versao WHERE script_rodado = '$entry' AND sistema='${nome_projeto}' OR (script_rodado = '$entry' AND sistema='ADEFINIR' );")

    if [ $vartest = 0 ]; then

        echo "Rodando script :: $entry"

        PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins -h  $servidor -p 5432 -L log_$entry.log  -a < $entry &> log_$entry.out.log

        RODADOSQL=$(cat log_$entry.out.log)

        flag=`echo $RODADOSQL|awk '{print match($0,"ERROR:")}'`

        if [ $flag = 0 ]; then

            PGPASSWORD=$password_db_jenkins  psql -d $dbname -U $username_db_jenkins -h $servidor -p 5432 -t -c "INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema)VALUES ('${NOME}', '$entry', '${nome_projeto}');" &> log_$entry.ins.out.log

            REGISTRAATUALIZACAO=$(cat log_$entry.ins.out.log)

            flag2=`echo $REGISTRAATUALIZACAO|awk '{print match($0,"ERROR:")}'`

            if [ $flag2 = 0 ]; then
              echo ""
              echo "#-----------------------------------------#"
              echo "   SQL registrado no controle de versao"
              echo "#-----------------------------------------#"
              echo ""
            else
              echo ""
              echo "#-------------------------------------------#"
              echo "#Erro ao registrar no controle de versao"
              echo "#-------------------------------------------#"
              echo ""
              echo $REGISTRAATUALIZACAO
              exit 1
            fi
        else
        	echo ""
            echo "#------------------------------------#"
            echo "FAILLLLLLLLLLLLL"
            echo "#------------------------------------#"
            echo ""
            echo ""
            echo "==============ERRO====================="
            echo $RODADOSQL|awk '{print match($0,"ERROR:")}'
            echo "==============ERRO====================="
            echo ""
            echo ""
            echo ""
            echo $RODADOSQL
            exit 1
        fi
     else
       echo ""
       echo "#---------------------------------------------------------------------------------#"
       echo "#Script $entry já tinha sido rodado no banco ${dbname} do servidor ${servidor}    #"
       echo "#---------------------------------------------------------------------------------#"
       echo ""
    fi
  else
	echo "DIRETORIO"
  fi
#echo $vartest
done