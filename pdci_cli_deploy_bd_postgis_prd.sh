#!/usr/bin/env bash

docker pull registry.gitlab.com/pdci/postgis:prd-latest

docker run --name db_base_full_latest -p 55432:5432    -v $(pwd)/postgres:/var/lib/postgresql/10/main  -e POSTGRES_DB=gis -e POSTGRES_USER=docker -e POSTGRES_PASS=docker -e ALLOW_IP_RANGE=0.0.0.0/0 -d  registry.gitlab.com/pdci/postgis:prd-latest

ID=$(docker ps -a | grep registry.gitlab.com/pdci/postgis | grep latest | awk '{print $1}')
echo "ID => ${ID}"
#cmd="docker commit ${ID}  registry.gitlab.com/pdci/postgis-prd:latest"
#exec_cmd=$(${cmd})
docker commit $ID  registry.gitlab.com/pdci/postgis:prd-latest
docker push registry.gitlab.com/pdci/postgis:prd-latest
#docker kill $ID
#docker rm $ID
##docker container prune -f
##Restaurar  o arquivo de produção em uma container do docker