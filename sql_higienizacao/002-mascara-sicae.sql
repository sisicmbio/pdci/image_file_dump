DO
$do$
BEGIN



  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sicae'
  )
  THEN

UPDATE sicae.usuario_externo
        SET no_usuario_externo = concat('no_usuario_externo'||sq_usuario_externo),
        tx_email = concat(sq_usuario_externo || '@gov.br');

UPDATE sicae.usuario_externo_dado_complementar 
        SET tx_endereco = concat('tx_endereco'||sq_usuario_externo),
        nu_telefone_celular = null,
        nu_telefone_fixo = null,
		co_cep=null;
		
		
UPDATE sicae.usuario_pessoa_fisica
        SET nu_registro_geral=sq_usuario_externo,
        nu_passaporte=sq_usuario_externo;
        

UPDATE sicae.usuario_pessoa_juridica 
        SET no_fantasia = concat('no_fantasia'||sq_usuario_externo);
        

--  UPDATE sicae.usuario_pessoa_fisica
--  SET nu_cpf='12345678909'
--  WHERE nu_cpf='69974349168';

UPDATE sicae.usuario_externo SET tx_senha=md5('qaz123');
UPDATE sicae.usuario SET tx_senha=md5('qaz123');

-- SELECT tx_url, REPLACE(tx_url, 'dsv.', '' ) FROM sicae.sistema where tx_url ilike '%//dsv.%';
-- UPDATE sicae.sistema SET tx_url=REPLACE(tx_url, 'dsv.', '' )  WHERE tx_url ilike '%//dsv.%';
--
-- SELECT tx_url, REPLACE(tx_url, 'tcti.', '' ) FROM sicae.sistema  where tx_url ilike '%//tcti.%';
-- UPDATE sicae.sistema SET tx_url=REPLACE(tx_url, 'tcti.', '' )  WHERE tx_url ilike '%//tcti.%';
--
-- SELECT tx_url, REPLACE(tx_url, 'dev.', '' ) FROM sicae.sistema  where tx_url ilike '%//dev.%';
-- UPDATE sicae.sistema SET tx_url=REPLACE(tx_url, 'dev.', '' )  WHERE tx_url ilike '%//dev.%';
--
-- SELECT tx_url, REPLACE(tx_url, 'hmg.', '' ) FROM sicae.sistema	where tx_url ilike '%//hmg.%';
-- UPDATE sicae.sistema SET tx_url= REPLACE(tx_url, 'hmg.', '' ) WHERE tx_url ilike '%//hmg.%';
--
-- SELECT tx_url, REPLACE(tx_url, 'https://', 'http://dev.' ) FROM sicae.sistema WHERE tx_url ilike 'https://%' AND  tx_url not ilike '%//dev.%';
-- UPDATE sicae.sistema SET tx_url= REPLACE(tx_url, 'https://', 'http://dev.' )  WHERE tx_url ilike 'https://%' AND  tx_url not ilike '%//dev.%';
--
-- SELECT tx_url, REPLACE(tx_url, 'http://', 'http://dev.' ) FROM sicae.sistema WHERE tx_url ilike 'http://%' AND  tx_url not ilike '%//dev.%';
-- UPDATE sicae.sistema SET tx_url= REPLACE(tx_url, 'http://', 'http://dev.' )  WHERE tx_url ilike 'http://%' AND  tx_url not ilike '%//dev.%';



  END IF;
  
  
  END 
  $do$
  
