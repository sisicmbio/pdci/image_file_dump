	UPDATE corporativo.cadastro_sem_cpf
   SET tx_justificativa = concat('tx_justificativa'||sq_cadastro_sem_cpf);

   
	UPDATE corporativo.vinculo_funcional
   SET nu_matricula = sq_vinculo_funcional; 
   
   
   
   UPDATE corporativo.telefone
   SET nu_telefone = null;
   
   
   
   UPDATE corporativo.profissional
   SET nu_matricula_siape = sq_profissional; 
   
   
   
   UPDATE corporativo.pessoa_juridica
   SET no_fantasia = concat('no_fantasia'||sq_pessoa);
   
   
   UPDATE corporativo.pessoa_fisica 
    SET no_mae = concat('no_mae'||sq_pessoa),    
    no_pai = concat('no_pai'||sq_pessoa),
	dt_nascimento='2019-01-01',
	nu_curriculo_lates=sq_pessoa; 
   
   
   
   UPDATE corporativo.pessoa 
    SET no_pessoa = concat('no_pessoa'||sq_pessoa); 
   
   
   UPDATE corporativo.incra_assentamento
   SET nu_benefic = nu_geocodig;
   
   
   UPDATE corporativo.dado_bancario
   SET nu_conta = concat('nu_conta'||sq_dado_bancario);
   
   
   
   UPDATE corporativo.email
   SET tx_email = concat(sq_email || '@gov.br');
   
  
   UPDATE corporativo.endereco
   SET tx_complemento = concat('tx_complemento'||sq_pessoa),
   tx_endereco = concat('tx_endereco'||sq_endereco),
   nu_endereco = sq_pessoa;
   
   
   UPDATE corporativo.endereco_estrangeiro 
	SET  tx_endereco_estrangeiro = concat('endereco_estrangeiro'||sq_endereco_estrangeiro);   
   
   
   
   UPDATE corporativo.documento
   SET tx_valor = concat('tx_valor'||sq_pessoa);


--  UPDATE corporativo.pessoa_fisica
--  SET nu_cpf='12345678909'
--  WHERE nu_cpf='69974349168';
