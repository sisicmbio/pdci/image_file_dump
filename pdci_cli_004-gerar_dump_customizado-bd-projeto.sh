#!/usr/bin/env bash

#echo "Numero de argumentos: $#"
#
#if [ $# -lt 1 ]; then
#   echo "Faltou passar  o caminho do registry como parametros"
#   exit 1
#fi

echo "Subindo container do postgis com o ultimo dump de producao"

#registry.gitlab.com/pdci/postgis:prd-latest
docker container prune -f
ID=$(docker ps | grep registry.gitlab.com/pdci/postgis:prd-latest | grep db_postgis_prd | awk '{print $1}')
echo ""
if [ -n ${ID} ]; then
    echo ""
    echo "Container com ultimo dump do banco de producao foi encontrado"
    echo ""
    echo "db_postgis_prd ID => ${ID}"
else
    echo "Subindo container com o ultimo dump de producao"
    docker run --name db_postgis_prd -p 55432:5432  -e POSTGRES_DB=gis -e POSTGRES_USER=docker -e POSTGRES_PASS=docker -e ALLOW_IP_RANGE=0.0.0.0/0 -d  registry.gitlab.com/pdci/postgis:prd-latest
    echo ""
    echo ""
fi
#Container com o dump da produção esta up

if [ -f dump/db-customizado.sql.gz ]; then
    echo ""
    echo "dump/db-customizado.sql.gz ja existe"
    echo ""

else

    echo ""
    echo ""
    echo "dump/db-customizado.sql.gz nao existe..."
    echo "Iniciar geracao de dump customizado ... "

    echo "verificando existencia de atualizacao da imagem base"
    docker pull registry.gitlab.com/pdci/image_file_dump:base
    echo ""
    echo ""

    echo "Rodando container registry.gitlab.com/pdci/image_file_dump:base para gerar arquivo dump customizado do projeto"
    echo "$(pwd)/conf:/conf"
    echo "$(pwd)/dump:/dump"
    docker run -v $(pwd)/conf:/conf -v $(pwd)/dump:/dump registry.gitlab.com/pdci/image_file_dump:base ler_db_e_customizar-image_file_dump-gerar-file.sh
    echo ""
    echo ""
fi



#com o arquivo dump disponivel em conf iremos criar a imagem_file_dump

##registry.gitlab.com/pdci/image_file_dump/image_file_dump_projeto-dev
##registry.gitlab.com/sisicmbio/app_infoconv
#nome_imagem_file_dump="image_file_dump"
#path_registry="$2/${nome_imagem_file_dump}:latest"
#file=$(pwd)/dump/db-customizado.sql.gz
#
#echo ""
#echo ""
#echo "nome_imagem_file_dump=${nome_imagem_file_dump}"
#
#echo ""
#echo ""
#echo "path_registry=${path_registry}"
#
#echo ""
#echo ""
#echo "file=${file}"
#
#
#
#if [ -f "$file"  ]; then
#    echo ""
#    echo "Construindo imagem_file_dump com o arquivo $file"
#    echo ""
#    docker container prune -f
#    echo ""
#    docker system prune -f
#    echo ""
#    echo "Registry a ser disponibilizada a imagem_file_dump do projeto:: $2"
#    echo ""
#    docker build -f Dockerfile -t ${path_registry} . --no-cache
#    echo ""
#    docker push ${path_registry}
#    echo ""
#else
#    echo ""
#    echo "***********************************************************"
#    echo "Arquivo /dump/db-customizado.sql.gz nao localizado.        "
#    echo "Operacao Cancelada                                         "
#    echo "***********************************************************"
#    echo ""
#fi
#
#
#ID=$(docker ps | grep registry.gitlab.com/pdci/postgis:prd-latest | grep db_postgis_prd | awk '{print $1}')
#echo "ID => ${ID}"
#cmd="docker commit ${ID}  registry.gitlab.com/pdci/app_laravel/postgis-dev:latest"
#
#docker commit ${ID}  registry.gitlab.com/pdci/app_laravel/postgis-dev:latest
#
##exec_cmd=$(${cmd})
##docker commit $ID  registry.gitlab.com/pdci/postgis-PRD:latest
#docker push registry.gitlab.com/pdci/app_laravel/postgis-dev:latest
##docker kill $ID
##docker rm $ID
###docker container prune -f
###Restaurar  o arquivo de produção em uma container do docker exemplo-pdci-004-customizar-bd-app_infocon