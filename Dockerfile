FROM alpine:3.9 as empty

RUN apk --no-cache add \
   tini \
   postgresql-client \
   bash \
   net-tools

RUN mkdir /pdci_logs/ \
 && mkdir /dump/ \
 && mkdir /sql/


RUN mkdir -p /contrib/pg-10/postgis-2.4/
RUN mkdir -p /contrib/pg-10/postgis-2.5/
COPY contrib /contrib
RUN chmod 777 -R /contrib


COPY scripts/ /bin/
WORKDIR /bin/
RUN  chmod +x /bin/*.sh

ENTRYPOINT ["/sbin/tini", "--","pdci-entrypoint.sh"]
#CMD ["bash"]

FROM empty as filedump
#COPY sql  /dump/ fazer logica na pasta script para ficar padrao
COPY dump /dump/
ENTRYPOINT ["/sbin/tini", "--","pdci-entrypoint.sh"]

FROM empty as sql
COPY sql  /sql/
ENTRYPOINT ["/sbin/tini", "--","pdci-entrypoint.sh"]
