#!/usr/bin/env bash

#docker-compose -f docker-compose-disponibilizar-imagem-db-prd-local.yml up
#
#docker container prune -f
#    - docker run --name db_base_full_latest -p 55432:5432   -e POSTGRES_DB=gis -e POSTGRES_USER=docker -e POSTGRES_PASS=docker -e ALLOW_IP_RANGE=0.0.0.0/0 -d  registry.gitlab.com/pdci/postgis:latest
ID=$(docker ps | grep registry.gitlab.com/pdci/postgis | grep latest | grep db_base_full_latest |  awk '{print $1}')
echo "ID => ${ID}"
docker ps
docker container inspect  "${ID}"
IP_DB=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${ID})

docker run -v $(pwd)/conf:/conf -e PDCI_DB_HOST=${IP_DB} registry.gitlab.com/pdci/image_file_dump/image_file_dump-prd:latest pdci_restaurar_db.sh

docker commit $ID  registry.gitlab.com/pdci/postgis:prd-latest
docker push registry.gitlab.com/pdci/postgis:prd-latest
