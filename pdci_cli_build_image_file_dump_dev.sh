#!/usr/bin/env bash
#docker container prune -f
#docker volume prune -f
#docker system prune -f
#
#
#if [ -e /dump/db-customizado.sql.gz ]; then
#docker pull registry.gitlab.com/pdci/image_file_dump/image_file_dump-prd:latest
#docker pull registry.gitlab.com/pdci/postgis:latest
#docker-compose -f docker-compose-customizar-db.yml up -d
#
#ID_image_file_dump-prd=$(docker ps -a | grep registry.gitlab.com/pdci/image_file_dump/image_file_dump-prd | grep latest | awk '{print $1}')
#echo "ID_image_file_dump => ${ID_image_file_dump}"
#$cmd="docker logs ${ID_image_file_dump} -f"
#$echo "cmd :: ${cmd}"
#$exec_cmd=$(${cmd})
#
##docker cp ${ID}:/db-customizado.tar.gz ./
#docker-compose -f docker-compose-customizar-db.yml down  --remove-orphans
#else
#
#echo "Não existe arquivo /dump/db-customizado.sql.gz. Favor rodar o processo de customizacao "
#
#fi
#
file=./dump/db-customizado.sql.gz
if [ -f "$file"  ]; then
    docker container prune -f
    docker system prune -f
    docker build -f Dockerfile -t registry.gitlab.com/pdci/image_file_dump/image_file_dump_projeto-dev:latest . --no-cache
    docker push registry.gitlab.com/pdci/image_file_dump/image_file_dump_projeto-dev:latest
else
    echo ""
    echo "***********************************************************"
    echo "Arquivo /dump/db-customizado.sql.gz nao localizado.        "
    echo "Operacao Cancelada                                         "
    echo "***********************************************************"
    echo ""
fi
