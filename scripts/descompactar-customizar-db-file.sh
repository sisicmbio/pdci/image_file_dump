#!/usr/bin/env bash
PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO="db-customizado.tar.gz"

esperar_conexao_db.sh

if [ -e "${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO}" ] ; then
    echo
    echo "Descompacta file ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO}"
    echo
    cmd="tar -xzvf ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO}"
    echo "cmd :: ${cmd}"
    ret_cmd=$(${cmd});
else
    echo
    echo "ERRO :: Arquivo compactado nao encontrado. ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO}"
    echo
fi