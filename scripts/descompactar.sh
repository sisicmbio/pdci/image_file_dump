#!/usr/bin/env sh

cat >&1 <<-'EOWARN'
				****************************************************
				INFO: Descompactar arquivo dump de producao

				****************************************************
			EOWARN
ls -lah /dump/*
tar xzvf /dump/*.tar.gz

cat >&1 <<-'EOWARN'
				****************************************************
				INFO: FIM do Descompactar arquivo dump de producao

				****************************************************
			EOWARN