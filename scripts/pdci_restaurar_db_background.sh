#!/usr/bin/env bash
export PDCI_DB_NAME_OLD=${PDCI_DB_NAME:-db_dev_cotec}
export PDCI_DB_NAME=${PDCI_DB_NAME}_tmp

echo "PDCI_DB_NAME_OLD = ${PDCI_DB_NAME_OLD}"
echo "PDCI_DB_NAME = ${PDCI_DB_NAME}"
sh /bin/esperar_conexao_db.sh
sh /bin/recriar_database.sh
sh /bin/init-import-file-dump.sh

echo ""
echo "RENOMEANDO BASE PDCI_DB_NAME_OLD :: ${PDCI_DB_NAME_OLD} PARA PDCI_DB_NAME:${PDCI_DB_NAME}"
echo ""

psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${PDCI_DB_NAME_OLD}'";
psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "ALTER DATABASE ${PDCI_DB_NAME_OLD} RENAME TO ${PDCI_DB_NAME_OLD}_BKP" ;
psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "ALTER DATABASE ${PDCI_DB_NAME} RENAME TO ${PDCI_DB_NAME_OLD}" ;