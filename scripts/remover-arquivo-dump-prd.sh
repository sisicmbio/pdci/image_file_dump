#!/usr/bin/env bash
#PDCI_DB_FILE_ROLES=create_user_docker_20181227-123001.sql
#PDCI_DB_FILE_DUMP=docker_20181227-123001.dump
cat >&1 <<-'EOWARN'
				**************************************************************
				INFO: Remover arquivo de dump e roles de producao
				**************************************************************
			EOWARN
nome_arquivo=$(ls docker*.dump | head -1)
nome_arquivo_user=$(ls create_user_docker*.sql | head -1)

if [ -e ${nome_arquivo} ]; then
    rm -vf ${nome_arquivo}
fi

if [ -e ${nome_arquivo_user} ]; then
    rm -vf ${nome_arquivo_user}
fi
