#!/usr/bin/env sh
#set -x
#set -e
#daasdasd
PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
PDCI_DB_USERNAME=${PDCI_DB_USERNAME:-postgres}
PDCI_DB_PASSWORD=${PDCI_DB_PASSWORD:-postgres}
PDCI_DB_NAME=${PDCI_DB_NAME:-db_dev_cotec}

echo "versao 1.97"
echo "APP_PROJETO => "${APP_PROJETO}" => ${APP_PROJETO}"
echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
echo "PDCI_DB_PORT => ${PDCI_DB_PORT}"
echo "PDCI_DB_USERNAME => ${PDCI_DB_USERNAME}"
echo "PDCI_DB_PASSWORD => ${PDCI_DB_PASSWORD}"
echo "PDCI_DB_NAME => ${PDCI_DB_NAME}"

esperar_conexao_db() {
    psql_on=$(PGPASSWORD=$PDCI_DB_PASSWORD  psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT}  -l 2>/dev/null | grep "List of databases")

    echo "psql_on => ${psql_on}"
    limit=1300
    count=0
    while [ -z "${psql_on}" ]; do
      if [ ${count} -lt ${limit} ]; then
        echo "****************************************************"
        echo "Esperando conexao com o banco (${PDCI_DB_HOST} : ${PDCI_DB_PORT}) em   ${count}/${limit} segundos"
        echo "****************************************************"
        echo ""
        let count=count+1
        sleep 1
        psql_on=$(PGPASSWORD=$PDCI_DB_PASSWORD  psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT}  -l 2>/dev/null | grep "List of databases")
        if [ -z "${psql_on}" ]; then
         echo "..."
        else
         echo ""
         echo "Conexao estabelecida com o banco de dados."
         echo ""
        fi
      else
        psql_on="ESGOTOU O TEMPO DE ESPERA"
        echo "Tempo esgotado de esperar pela conexao ao banco de dados"
      fi
    done
}
rm -rf /script_out/*
mkdir -p /script_out/
rm -rf /log_sql/*
mkdir -p /log_sql/log_/sql/

echo ""
echo " $0: "
echo ""

echo ""
echo "Cria schema jenkins e usuario usr_jenkins  caso nao exista."
echo ""

PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "DO \$do$ BEGIN IF NOT EXISTS( SELECT  FROM   pg_catalog.pg_roles  WHERE  rolname = 'usr_jenkins' ) THEN  create user usr_jenkins with password 'usr_jenkins';  END IF;  END; \$do$;"

PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';"

vartest=$(PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';")

#echo "vartest=> ${vartest}"

if [ $vartest -eq 0 ]; then

  echo ""
  echo "*******************************************************"
  echo "  Criando estrutura do jenkins por não existir. "
  echo "*******************************************************"
  echo ""
  PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c  "CREATE SCHEMA IF NOT EXISTS jenkins AUTHORIZATION ${PDCI_DB_USERNAME};"
  PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c  "COMMENT ON SCHEMA jenkins IS 'Tabela responsavel em controlar as atualizações de sql no banco de dados do ambiente';"
  PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c  "CREATE TABLE jenkins.controle_versao (  des_tag text NOT NULL,  script_rodado text NOT NULL,  data_inclusao text NOT NULL DEFAULT now(),  ordem serial NOT NULL,  sistema text NOT NULL,username text NOT NULL,ticket text, obs text, CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, script_rodado)) WITH (  OIDS=FALSE ); GRANT ALL ON TABLE jenkins.controle_versao TO usr_jenkins; COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado'; COMMENT ON COLUMN jenkins.controle_versao.sistema IS 'tabela com o nome do projeto'; CREATE INDEX controle_versao_pk_i  ON jenkins.controle_versao  USING btree  (sistema COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"
  #PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c  "ALTER TABLE jenkins.controle_versao OWNER TO ${PDCI_DB_USERNAME};"
  PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c  "COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado';"

  #PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "CREATE INDEX controle_versao_pk_i ON jenkins.controle_versao USING btree (des_tag COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"
  PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "ALTER ROLE ${PDCI_DB_USERNAME} SET search_path TO jenkins;"

fi

if [ ! -z ${PDCI_MONITORA_VIRTUAL_HOST} ]; then
      echo ""
      echo "*********************************************************************************************"
      echo ""
      echo "    Lendo variaveis PDCI_MONITORA_VIRTUAL_HOST "
      echo ""
      echo "        Alterando a url para ${PDCI_MONITORA_VIRTUAL_HOST} no sicae."
      echo ""
      echo "*********************************************************************************************"
      echo ""

    #PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "UPDATE sicae.sistema SET tx_url='http://${PDCI_MONITORA_VIRTUAL_HOST}/login/' WHERE tx_url ilike 'http://dev.monitora.sisicmbio.icmbio.gov.br/login/';"

    PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "DO \$do$ BEGIN IF EXISTS ( SELECT  FROM information_schema.tables  WHERE table_schema='sicae' AND table_name='sistema' AND table_catalog='$PDCI_DB_NAME' ) THEN UPDATE sicae.sistema SET tx_url='http://${PDCI_MONITORA_VIRTUAL_HOST}/login/' WHERE tx_url ilike 'http://dev.monitora.sisicmbio.icmbio.gov.br/login/'; END IF; END; \$do$;"
fi

if [ ! -z ${PDCI_SICAE_VIRTUAL_HOST} ]; then
      echo ""
      echo "*********************************************************************************************"
      echo ""
      echo "    Lendo variaveis PDCI_SICAE_VIRTUAL_HOST "
      echo ""
      echo "        Alterando a url do ${PDCI_SICAE_VIRTUAL_HOST} no sicae."
      echo ""
      echo "*********************************************************************************************"
      echo ""

    #PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "UPDATE sicae.sistema SET tx_url='http://${PDCI_SICAE_VIRTUAL_HOST}' WHERE tx_url = 'http://dev.sicae.sisicmbio.icmbio.gov.br';"
    echo ""
   PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "DO \$do$ BEGIN IF EXISTS ( SELECT  FROM information_schema.tables  WHERE table_schema='sicae' AND table_name='sistema' AND table_catalog='$PDCI_DB_NAME' ) THEN UPDATE sicae.sistema SET tx_url='http://${PDCI_SICAE_VIRTUAL_HOST}' WHERE tx_url ilike 'http://dev.sicae.sisicmbio.icmbio.gov.br'; END IF; END; \$do$;"

fi

#export > export.txt
#
#cat export.txt >&1

cat >&1 <<-'EOWARN'
				****************************************************
				INFO: Processo de atualizacao do banco de dados /sql/:
				****************************************************
			EOWARN

#psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} < $f
cd /
for f in /sql/*; do
    echo "f => $f"
    case "$f" in
        *.sh)
            # https://github.com/docker-library/postgres/issues/450#issuecomment-393167936
            # https://github.com/docker-library/postgres/pull/452
            if [ -x "$f" ]; then
                echo "$0: running $f"
                "$f"
            else
                echo "$0: sourcing $f"
                . "$f"
            fi
            ;;
        *.sql)
            echo "$0: running $f";
            echo

            esperar_conexao_db

            if [ -f "$f" ]; then
 echo "Rodando script sql :: $f"
            vartest=$(PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME  -h $PDCI_DB_HOST -p 5432 -t -c "SELECT count(*) FROM jenkins.controle_versao WHERE script_rodado = replace( '$f','/sql/','sql/') AND sistema='${APP_PROJETO:-app}' ;")

            if [ $vartest = 0 ]; then
                _NOME_FILE_LOG=$(echo "$f" | sed -e "s|/|_|g")

                PGPASSWORD=$PDCI_DB_PASSWORD psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME -v PDCI_DB_NAME=${PDCI_DB_NAME} -v PDCI_DB_USERNAME=${PDCI_DB_USERNAME} -h  $PDCI_DB_HOST -p 5432 -L /log_sql/${_NOME_FILE_LOG}.log  -a < "$f" &> /log_sql/${_NOME_FILE_LOG}.out.log
#                echo
#                echo "/log_sql/${_NOME_FILE_LOG}.out.log"
#                echo
                RODADOSQL=$(cat "/log_sql/${_NOME_FILE_LOG}.out.log")

                flag=`echo $RODADOSQL|awk '{print match($0,"ERROR:")}'`

                if [ $flag = 0 ]; then

                    PGPASSWORD=$PDCI_DB_PASSWORD  psql -d $PDCI_DB_NAME -U $PDCI_DB_USERNAME -h $PDCI_DB_HOST -p 5432 -t -c "INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username) VALUES ('${NOME:-develop}', replace( '$f','/sql/','sql/'), '${APP_PROJETO:-app}','developer');" &> /log_sql/${_NOME_FILE_LOG}.ins.out.log

                    REGISTRAATUALIZACAO=$(cat /log_sql/${_NOME_FILE_LOG}.ins.out.log)

                    flag2=`echo $REGISTRAATUALIZACAO|awk '{print match($0,"ERROR:")}'`

                    if [ $flag2 = 0 ]; then
                      echo ""
                      echo "#-----------------------------------------#"
                      echo "   SQL registrado no controle de versao    "
                      echo "#-----------------------------------------#"
                      echo ""
                    else
                      echo ""
                      echo "#--ERROR--ERROR--ERROR--ERROR--ERROR--ERROR-#"
                      echo "#  ERROR ao registrar no controle de versao do jenkins  "
                      echo "#-------------------------------------------#"
                      echo ""
                      echo $REGISTRAATUALIZACAO
                      exit 1
                    fi
                else
                    echo ""
                    echo "#-------------------------------------------------------#"
                    echo ""
                    echo "        ERRRO AO RODAR SCRIPT DE BANCO DE DADOS                 "
                    echo ""
                    echo "#-------------------------------------------------------#"
                    echo ""
                    echo ""
#                    echo "==============    ERROR:  ====================="
#                    echo ""
#                    echo $RODADOSQL|awk '{print match($0,"ERROR:")}'
#                    echo ""
#                    echo "==============   ERROR:   ====================="
                    echo ""
                    echo ""
                    echo ""
                    echo $RODADOSQL
                    echo ""
                    echo ""
                    echo ""
                    exit 1
                fi
             else
               echo ""
               echo "#-----------------------------------------------------------------------------------------------------------------#"
               echo ""
               echo "     Script $f já tinha sido rodado no banco ${PDCI_DB_NAME} do PDCI_DB_HOST ${PDCI_DB_HOST}     "
               echo ""
               echo "#--------------------------------------------------------------------------------------------------------------- -#"
               echo ""
            fi
            else
            echo "DIRETORIO"
            fi


        ;;
        *.dump)
            esperar_conexao_db
            echo "$0: running $f";
            echo
            psql -U postgres  -v ON_ERROR_STOP=1  -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} < $f
        ;;
        *.gz)
            esperar_conexao_db
            echo "$0: running $f";
            gunzip -c "$f" | psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} ;
            echo
        ;;
        *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

cat >&1 <<-'EOWARN'
				******************************************************************
				     INFO: Fim da atualizacao do banco de dados  /sql/:
				******************************************************************
			EOWARN



##!/usr/bin/env sh
#set -xe
#
#PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
#PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
#PDCI_DB_USERNAME=${PDCI_DB_USERNAME:-postgres}
#PDCI_DB_PASSWORD=${PDCI_DB_PASSWORD:-postgres}
#PDCI_DB_NAME=${PDCI_DB_NAME:-db_dev_cotec}
#
#echo "versao 2.1"
#echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
#echo "PDCI_DB_PORT => ${PDCI_DB_PORT}"
#echo "PDCI_DB_USERNAME => ${PDCI_DB_USERNAME}"
#echo "PDCI_DB_PASSWORD => ${PDCI_DB_PASSWORD}"
#echo "PDCI_DB_NAME => ${PDCI_DB_NAME}"
#
#esperar_conexao_db() {
#    psql_on=$(PGPASSWORD=$PDCI_DB_PASSWORD  psql -U postgres -w -h "${PDCI_DB_HOST}" -p  "${PDCI_DB_PORT}"  -l 2>/dev/null | grep "List of databases")
#
#    echo "psql_on => ${psql_on}"
#    limit=1200
#    count=0
#    while [ -z "${psql_on}" ]; do
#      if [ ${count} -lt ${limit} ]; then
#        echo "****************************************************"
#        echo "Esperando conexao com o banco (${PDCI_DB_HOST} : ${PDCI_DB_PORT}) em   ${count}/${limit} segundos"
#        echo "****************************************************"
#        echo ""
#        # shellcheck disable=SC2039
#        let count=count+1
#        sleep 1
#        psql_on=$(PGPASSWORD=$PDCI_DB_PASSWORD  psql -U postgres -w -h "${PDCI_DB_HOST}" -p  "${PDCI_DB_PORT}"  -l 2>/dev/null | grep "List of databases")
#        if [ -z "${psql_on}" ]; then
#         echo "..."
#        else
#         echo ""
#         echo "Conexao estabelecida com o banco de dados."
#         echo ""
#        fi
#      else
#        psql_on="ESGOTOU O TEMPO DE ESPERA"
#        echo "Tempo esgotado de esperar pela conexao ao banco de dados"
#      fi
#    done
#}
#rm -rf /script_out/*
#mkdir -p /script_out/
#rm -rf /log_sql/*
#mkdir -p /log_sql/log_/sql/
#
#echo ""
#echo " $0: "
#echo ""
#
#echo ""
#echo "Cria schema jenkins e usuario usr_jenkins  caso nao exista."
#echo ""
#
#PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c "DO \$do$ BEGIN IF NOT EXISTS( SELECT  FROM   pg_catalog.pg_roles  WHERE  rolname = 'usr_jenkins' ) THEN  create user usr_jenkins with password 'usr_jenkins';  END IF;  END; \$do$;"
#
#PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';"
#
#vartest=$(PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';")
#
##echo "vartest=> ${vartest}"
#
## shellcheck disable=SC2086
#if [ $vartest -eq 0 ]; then
#
#  echo ""
#  echo "*******************************************************"
#  echo "  Criando estrutura do jenkins por não existir. "
#  echo "*******************************************************"
#  echo ""
#  PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c  "CREATE SCHEMA IF NOT EXISTS jenkins AUTHORIZATION ${PDCI_DB_USERNAME};"
#  PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c  "COMMENT ON SCHEMA jenkins IS 'Tabela responsavel em controlar as atualizações de sql no banco de dados do ambiente';"
#  PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c  "CREATE TABLE jenkins.controle_versao (  des_tag text NOT NULL,  script_rodado text NOT NULL,  data_inclusao text NOT NULL DEFAULT now(),  ordem serial NOT NULL,  sistema text NOT NULL, CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, script_rodado)) WITH (  OIDS=FALSE ); GRANT ALL ON TABLE jenkins.controle_versao TO usr_jenkins; COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado'; COMMENT ON COLUMN jenkins.controle_versao.sistema IS 'tabela com o nome do projeto'; CREATE INDEX controle_versao_pk_i  ON jenkins.controle_versao  USING btree  (sistema COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"
#  #PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c  "ALTER TABLE jenkins.controle_versao OWNER TO ${PDCI_DB_USERNAME};"
#  PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c  "COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado';"
#
#  #PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c "CREATE INDEX controle_versao_pk_i ON jenkins.controle_versao USING btree (des_tag COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"
#  PGPASSWORD=$PDCI_DB_PASSWORD psql -d "${PDCI_DB_NAME}" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c "ALTER ROLE ${PDCI_DB_USERNAME} SET search_path TO jenkins;"
#
#fi
#
#
#if [ ! -z ${PDCI_MONITORA_VIRTUAL_HOST} ]; then
#      echo ""
#      echo "*********************************************************************************************"
#      echo ""
#      echo "    Lendo variaveis PDCI_MONITORA_VIRTUAL_HOST "
#      echo ""
#      echo "        Alterando a url para ${PDCI_MONITORA_VIRTUAL_HOST} no sicae."
#      echo ""
#      echo "*********************************************************************************************"
#      echo ""
#
#    PGPASSWORD=$PDCI_DB_PASSWORD psql -d "$PDCI_DB_NAME" -U "$PDCI_DB_USERNAME"  -h "$PDCI_DB_HOST" -p 5432 -t -c "DO \$do$ BEGIN IF EXISTS ( SELECT  FROM information_schema.tables  WHERE table_schema='sicae' AND table_name='sistema' AND table_catalog='$PDCI_DB_NAME' ) THEN UPDATE sicae.sistema SET tx_url='http://${PDCI_MONITORA_VIRTUAL_HOST}/login/' WHERE tx_url ilike 'http://dev.monitora.sisicmbio.icmbio.gov.br/login/'; END IF; END; \$do$;"
#fi
#
## shellcheck disable=SC2236
#if [ ! -z ${PDCI_SICAE_VIRTUAL_HOST} ]; then
#      echo ""
#      echo "*********************************************************************************************"
#      echo ""
#      echo "    Lendo variaveis PDCI_SICAE_VIRTUAL_HOST "
#      echo ""
#      echo "        Alterando a url do ${PDCI_SICAE_VIRTUAL_HOST} no sicae."
#      echo ""
#      echo "*********************************************************************************************"
#      echo ""
#      echo ""
#
#   PGPASSWORD=$PDCI_DB_PASSWORD psql \
#   -d "$PDCI_DB_NAME" \
#   -U "$PDCI_DB_USERNAME" \
#   -h "$PDCI_DB_HOST" \
#   -p 5432 \
#   -t -c "DO
#   \$do$
#     BEGIN
#        IF EXISTS ( SELECT
#                      FROM information_schema.tables
#                    WHERE table_schema='sicae'
#                      AND table_name='sistema'
#                      AND table_catalog='${PDCI_DB_NAME}'
#                    ) THEN
#          UPDATE sicae.sistema SET tx_url='http://${PDCI_SICAE_VIRTUAL_HOST}'
#           WHERE tx_url ilike 'http://dev.sicae.sisicmbio.icmbio.gov.br';
#        END IF;
#     END;
#   \$do$;"
#
#fi
#
##export > export.txt
##
##cat export.txt >&1
#
#cat >&1 <<-'EOWARN'
#				****************************************************
#				INFO: Processo de atualizacao do banco de dados /sql/:
#				****************************************************
#			EOWARN
#
##psql -U postgres -w -h "${PDCI_DB_HOST}" -p  "${PDCI_DB_PORT}" -d ${PDCI_DB_NAME} < $f
#cd /
#for f in /sql/*; do
#    echo "f => $f"
#    case "$f" in
#        *.sh)
#            if [ -x "$f" ]; then
#                echo "$0: running $f"
#                "$f"
#            else
#                echo "$0: sourcing $f"
#                . "$f"
#            fi
#            ;;
#        *.sql)
#            echo "$0: running $f";
#            echo
#
#            esperar_conexao_db
#
#            if [ -f $f ]; then
#              echo "Rodando script sql :: $f"
#              vartest=$(PGPASSWORD=$PDCI_DB_PASSWORD psql -d "$PDCI_DB_NAME" -U  "${PDCI_DB_USERNAME}"  -h "${PDCI_DB_HOST}" -p 5432 -t -c "SELECT count(*) FROM jenkins.controle_versao WHERE script_rodado = replace( '$f','/sql/','sql/') AND sistema='${APP_PROJETO:-app}' ;")
#
#            # shellcheck disable=SC2086
#            if [ $vartest = 0 ]; then
#                _NOME_FILE_LOG=$(echo $f | sed -e "s|/|_|g")
##                (
##                 echo "\i $line;
##                 INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username)
##                        VALUES ('${NOME}', '${line}', 'app_ojs','${BUILD_USER}');
##                        "
##                ) | PGPASSWORD=${PDCI_DB_PASSWORD} psql \
##                  -v ON_ERROR_STOP=1 \
##                  -v PDCI_DB_NAME="${PDCI_DB_NAME}" \
##                  ${PDCI_PSQL_PARAMETROS} \
##                  -At \
##                  -h ${PDCI_DB_HOST} \
##                  -p 5432 \
##                  -U ${PDCI_DB_USERNAME} \
##                  -d ${PDCI_DB_NAME}
#
#
#                PGPASSWORD=$PDCI_DB_PASSWORD psql \
#                -d "${PDCI_DB_NAME}" \
#                -U "${PDCI_DB_USERNAME}" \
#                -h "${PDCI_DB_HOST}" \
#                ${PDCI_PSQL_PARAMETROS} \
#                -p 5432 \
#                -v PDCI_DB_NAME="${PDCI_DB_NAME}" \
#                -v PDCI_DB_USERNAME="${PDCI_DB_USERNAME}" \
#                -L /log_sql/${_NOME_FILE_LOG}.log  \
#                -a < $f &> /log_sql/${_NOME_FILE_LOG}.out.log
#                echo
#                echo "/log_sql/${_NOME_FILE_LOG}.out.log"
#                echo
#                RODADOSQL=$(cat /log_sql/${_NOME_FILE_LOG}.out.log)
#
#                flag=$(echo "$RODADOSQL" | awk '{print match($0,"ERROR:")}')
#
#                if [ $flag = 0 ]; then
#
#                    PGPASSWORD=$PDCI_DB_PASSWORD  psql \
#                    -d "${PDCI_DB_NAME}" \
#                    -U  "${PDCI_DB_USERNAME}" \
#                    -h "${PDCI_DB_HOST}" \
#                    -p 5432 \
#                    -t -c "INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username)
#                     VALUES ('${NOME}', replace( '$f','/sql/','sql/'), '${APP_PROJETO:-app}','developer');" &> /log_sql/${_NOME_FILE_LOG}.ins.out.log
#
#                    REGISTRAATUALIZACAO=$(cat /log_sql/"${_NOME_FILE_LOG}".ins.out.log)
#
#                    flag2=$(echo "$REGISTRAATUALIZACAO" | awk '{print match($0,"ERROR:")}')
#
#                    # shellcheck disable=SC2086
#                    if [ $flag2 = 0 ]; then
#                      echo ""
#                      echo "#-----------------------------------------#"
#                      echo "   SQL registrado no controle de versao    "
#                      echo "#-----------------------------------------#"
#                      echo ""
#                    else
#                      echo ""
#                      echo "#--ERROR--ERROR--ERROR--ERROR--ERROR--ERROR-#"
#                      echo "#  ERROR ao registrar no controle de versao do jenkins  "
#                      echo "#-------------------------------------------#"
#                      echo ""
#                      echo "${REGISTRAATUALIZACAO}"
#                      exit 1
#                    fi
#                else
#                    echo ""
#                    echo "#-------------------------------------------------------#"
#                    echo ""
#                    echo "        ERRRO AO RODAR SCRIPT DE BANCO DE DADOS                 "
#                    echo ""
#                    echo "#-------------------------------------------------------#"
#                    echo ""
#                    echo ""
##                    echo "==============    ERROR:  ====================="
##                    echo ""
##                    echo $RODADOSQL|awk '{print match($0,"ERROR:")}'
##                    echo ""
##                    echo "==============   ERROR:   ====================="
#                    echo ""
#                    echo ""
#                    echo ""
#                    echo "${RODADOSQL}"
#                    echo ""
#                    echo ""
#                    echo ""
#                    exit 1
#                fi
#             else
#               echo ""
#               echo "#-----------------------------------------------------------------------------------------------------------------#"
#               echo ""
#               echo "     Script ${f} já tinha sido rodado no banco ${PDCI_DB_NAME} do PDCI_DB_HOST \"${PDCI_DB_HOST}\"     "
#               echo ""
#               echo "#--------------------------------------------------------------------------------------------------------------- -#"
#               echo ""
#            fi
#            else
#            echo "DIRETORIO"
#            fi
#
#
#        ;;
#        *.dump)
#            esperar_conexao_db
#            echo "$0: running $f";
#            echo
#            psql -U postgres -w -h "${PDCI_DB_HOST}" -p  "${PDCI_DB_PORT}" -d "${PDCI_DB_NAME}"  < $f
#        ;;
#        *.gz)
#            esperar_conexao_db
#            echo "$0: running $f";
#            gunzip -c $f | psql -U postgres -w -h "${PDCI_DB_HOST}" -p  "${PDCI_DB_PORT}" -d "${PDCI_DB_NAME}" ;
#            echo
#        ;;
#        *)        echo "$0: ignoring $f" ;;
#    esac
#    echo
#done
#
#cat >&1 <<-'EOWARN'
#				******************************************************************
#				     INFO: Fim da atualizacao do banco de dados  /sql/:
#				******************************************************************
#			EOWARN