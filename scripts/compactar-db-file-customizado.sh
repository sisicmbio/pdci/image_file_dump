#!/usr/bin/env bash
PDCI_ARQUIVO_DUMP_CUSTOMIZADO="/dump/db-customizado.sql"
PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO="db-customizado.tar.gz"

if [ -e "${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}" ] ; then
    echo
    echo "Compactando arquivo de dump customizado."
    echo
    cmd_dmp="tar -czvf ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO} ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}"
    echo
    echo "cmd_dmp = ${cmd_dmp}"
    exclude_dump=$(${cmd_dmp})
    echo
    echo
    echo "File customizado compactado com sucesso!"
    echo

else
    echo
    echo "ERRO: Arquivo ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO} não encontrado"
    echo
fi

    echo
    echo "Movendo arquivo ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO} para conf/. Ficará disponvel ao host!"
    echo

mv -v ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO_COMPACTADO} /conf/