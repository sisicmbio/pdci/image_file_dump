#!/usr/bin/env bash

PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
PDCI_DB_NAME=db_dev_cotec
PDCI_REGISTRY_BD_DES=${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV
PDCI_ARQUIVO_SCHEMAS="/conf/db.schemas"
PDCI_ARQUIVO_DUMP_CUSTOMIZADO="/dump/db-customizado.sql"

cat >&1 <<-'EOWARN'
				**************************************************************
				INFO: Customizar db conforme arquivo /conf/db.schemas
				**************************************************************
			EOWARN

esperar_conexao_db.sh


if [ -e "${PDCI_ARQUIVO_SCHEMAS}" ] ; then
    echo
    echo "Existe arquivo de customizacao de banco no projeto. ${PDCI_ARQUIVO_SCHEMAS}"
    echo

    echo "o arquivo ${PDCI_ARQUIVO_SCHEMAS} existe"
    schemas=$(cat ${PDCI_ARQUIVO_SCHEMAS})
    echo "**********************************************************************"
    echo "Informados pelo programador que o sistemas utiliza ${schemas}"
    echo "**********************************************************************"

    comando_sql_include="select string_agg(''''||tb5.app_consome_schema||'''', ', ') as app_consome_schema from (SELECT distinct source_ns.nspname as app_consome_schema FROM pg_depend JOIN pg_rewrite ON pg_depend.objid = pg_rewrite.oid JOIN pg_class as dependent_view ON pg_rewrite.ev_class = dependent_view.oid JOIN pg_class as source_table ON pg_depend.refobjid = source_table.oid JOIN pg_attribute ON pg_depend.refobjid = pg_attribute.attrelid     AND pg_depend.refobjsubid = pg_attribute.attnum JOIN pg_namespace dependent_ns ON dependent_ns.oid = dependent_view.relnamespace JOIN pg_namespace source_ns ON source_ns.oid = source_table.relnamespace WHERE dependent_ns.nspname in (select schema_name from information_schema.schemata where schema_name in ('pg_toast','pg_temp_1','pg_toast_temp_1','pg_catalog','public','information_schema','tiger','tiger_data','topology',${schemas})) UNION select distinct schema_name as app_consome_schema from information_schema.schemata where schema_name in ('pg_toast','pg_temp_1','pg_toast_temp_1','pg_catalog','public','information_schema','tiger','tiger_data','topology', ${schemas})) as tb5;"
    echo
    #echo "comando_sql_include=> ${comando_sql_include}"
    sql_schema="psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -t -c \"${comando_sql_include}\" "
    echo "**********************************************************************"
    echo "sql_schema=> ${sql_schema}"
    echo "**********************************************************************"
    #apenas os schemas


    include_schema=$(psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -t -c "${comando_sql_include}")
    echo "**********************************************************************"
    echo "Relacao de esquemas dependentes ou obrigatorios, informados pelo jenkins, atravês do processo de descobertas de dependência do pdci = ${include_schema}"
    echo "**********************************************************************"
    echo
    cmd_sql_exclude="select string_agg('--exclude-schema='||schema_name, ' ') as app_consome_schema from information_schema.schemata where schema_name not in (${include_schema});"
    echo
    echo "comando_sql_exclude=> ${cmd_sql_exclude}"
    exclude_schema=$(psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -t -c "${cmd_sql_exclude}")
    echo
    echo "exclude_schema = ${exclude_schema}"
    echo

##    cmd_dump_exclude_schema="pg_dump -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT}    ${exclude_schema}  --disable-triggers -d ${PDCI_DB_NAME}  -f ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO} "
#    cmd_dump_exclude_schema="pg_dump -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT}    ${exclude_schema}  --disable-triggers -d ${PDCI_DB_NAME} | gzip > /dump/db-customizado.sql.gz  "
#    echo
#    echo "Comando :: ${cmd_dump_exclude_schema}"
#    exclude_dump=$(${cmd_dump_exclude_schema})
#    echo
#    echo
    echo "*********************************************************************"
    echo " Gerando Dump customizado                                            "
    echo "*********************************************************************"
    echo
    pg_dump -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} ${exclude_schema}  --disable-triggers -d ${PDCI_DB_NAME} | gzip > /dump/db-customizado.sql.gz ;

    echo "*********************************************************************"
    echo " Dump Customizado Finalizado                                           "
    echo "*********************************************************************"
    echo
ls -lah /dump/


else
	echo
	echo "o arquivo ${PDCI_ARQUIVO_SCHEMAS} não existe. Sem personalizacao de banco"
    echo
fi

