#!/usr/bin/env bash
set -Eeo pipefail

file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

#if [ "$1" != "bash" ]; then
# echo "#!/usr/bin/env bash" > /bin/run_comandos_pdci.sh
# exec "chmod +x /bin/run_comandos_pdci.sh"
#    for var in "$@"
#    do
#        if [ -x "$var" ]; then
#            echo "$0: gerando script com o comando PDCI:"
#            echo "$var" >> run_comandos_pdci.sh
##            echo " * $var"
#            #"$var"
#            exec "$var"
#            echo ""
#
#        else
#            echo "$0: codigo $var"
#            . "$var"
#        fi
#    done
#else
#exec "$@"
#fi

exec $@

#sh /bin/esperar_conexao_db.sh
#sh /bin/descompactar.sh
#sh /bin/remover-arquivo-dump-prd-compactado.sh
#sh /bin/esperar_conexao_db.sh
#sh /bin/recriar_database.sh
#sh /bin/init-import-file-dump.sh
#sh /bin/remover-arquivo-dump-prd.sh
#sh /bin/customizar-db-gera-file.sh
#sh /bin/compactar-db-file-customizado.sh
#sh /bin/remover-arquivo-dump-customizado.sh
#sh /bin/recriar_database.sh