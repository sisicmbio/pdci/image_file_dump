#!/usr/bin/env bash
#Script para criar base vazia inicialmente e restaura dump customizado.
PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
PDCI_DB_USERNAME=${PDCI_DB_USERNAME:-postgres}
PDCI_DB_PASSWORD=${PDCI_DB_PASSWORD:-postgres}
PDCI_DB_NAME=${PDCI_DB_NAME:-db_dev_cotec}


echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
echo "PDCI_DB_PORT => ${PDCI_DB_PORT}"
echo "PDCI_DB_USERNAME => ${PDCI_DB_USERNAME}"
echo "PDCI_DB_PASSWORD => ${PDCI_DB_PASSWORD}"
echo "PDCI_DB_NAME => ${PDCI_DB_NAME}"


echo ""
echo "$0: "
echo ""

for i in db_dev_pdci ;
#for i in ${PDCI_DB_NAME} db_dev_sigeo ;
#for i in ${PDCI_DB_NAME} ;
do
	PDCI_DB_NAME=${i}

  echo ""
  echo "Finalizando conexoes ativas no base de dados ${PDCI_DB_NAME} caso nao exista."
  echo ""

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${PDCI_DB_NAME}'";


  echo ""
  echo "Recriando base de dados ${PDCI_DB_NAME} caso exista."
  echo ""

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "DROP DATABASE IF EXISTS ${PDCI_DB_NAME}" ;

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "CREATE DATABASE ${PDCI_DB_NAME}";

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "ALTER ROLE postgres RESET search_path;";



  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${PDCI_DB_NAME}'";


  echo ""
  echo "Cria usuario usr_jenkins caso nao exista."
  echo ""

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "DO \$do$ BEGIN IF NOT EXISTS( SELECT  FROM   pg_catalog.pg_roles  WHERE  rolname = 'usr_jenkins' ) THEN  create user usr_jenkins with password 'usr_jenkins';  END IF;  END; \$do$;";

  echo ""
  echo "Cria extensions  caso nao exista."
  echo ""


  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS tablefunc CASCADE;";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS pg_trgm CASCADE;";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS fuzzystrmatch CASCADE;";

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS postgis CASCADE;";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS postgis_topology CASCADE;";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS postgis_tiger_geocoder CASCADE;";

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "create extension IF NOT EXISTS adminpack CASCADE;";

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "DROP TABLESPACE  IF EXISTS tbs_index_ssd;";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "CREATE TABLESPACE tbs_index_ssd LOCATION '/index_ssd';";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "DROP TABLESPACE  IF EXISTS tbs_geo;";
  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "CREATE TABLESPACE tbs_geo LOCATION '/geo';";

  echo ""
  echo ""
  echo "Rodar script conforme : https://spatialreference.org/ref/sr-org/6820/postgis/"
  echo ""
  echo "INSERT into spatial_ref_sys (srid, auth_name, auth_srid, proj4text, srtext) values ( 96820, 'sr-org', 6820, '+proj=aea +lat_1=-2 +lat_2=-22 +lat_0=-12 +lon_0=-54 +x_0=0 +y_0=0 +a=6378160 +b=6356537.557298475 +units=m +no_defs ', 'PROJCS[\"Albers Conical Equal Area (Brazil)\", GEOGCS[\"GCS_South_American_1969\", DATUM[\"D_South_American_1969\", SPHEROID[\"GRS_1967_Truncated\", 6378160.0,294.9786982]], PRIMEM[\"Greenwich\",0.0], UNIT[\"Degree\",0.0174532925199433]], PROJECTION[\"Albers_Conic_Equal_Area\"], PARAMETER[\"False_Easting\",0.0], PARAMETER[\"False_Northing\",0.0], PARAMETER[\"Central_Meridian\",-54.0], PARAMETER[\"Standard_Parallel_1\",-2.0], PARAMETER[\"Standard_Parallel_2\",-22.0], PARAMETER[\"Latitude_Of_Origin\",-12.0], UNIT[\"Meter\",1.0]]');"
  echo ""

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} -c "INSERT into spatial_ref_sys (srid, auth_name, auth_srid, proj4text, srtext) values ( 96820, 'sr-org', 6820, '+proj=aea +lat_1=-2 +lat_2=-22 +lat_0=-12 +lon_0=-54 +x_0=0 +y_0=0 +a=6378160 +b=6356537.557298475 +units=m +no_defs ', 'PROJCS[\"Albers Conical Equal Area (Brazil)\", GEOGCS[\"GCS_South_American_1969\", DATUM[\"D_South_American_1969\", SPHEROID[\"GRS_1967_Truncated\", 6378160.0,294.9786982]], PRIMEM[\"Greenwich\",0.0], UNIT[\"Degree\",0.0174532925199433]], PROJECTION[\"Albers_Conic_Equal_Area\"], PARAMETER[\"False_Easting\",0.0], PARAMETER[\"False_Northing\",0.0], PARAMETER[\"Central_Meridian\",-54.0], PARAMETER[\"Standard_Parallel_1\",-2.0], PARAMETER[\"Standard_Parallel_2\",-22.0], PARAMETER[\"Latitude_Of_Origin\",-12.0], UNIT[\"Meter\",1.0]]');";

  echo ""
  echo "Importar legacy"
  echo ""

  PGPASSWORD=$PDCI_DB_PASSWORD psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} < /contrib/pg-10/postgis-2.5/legacy.sql

  echo ""
  echo "##############################################################################################"
  echo ""
  echo "    Finalizando processo de criação e configuração do banco de dados: ${PDCI_DB_NAME} "
  echo ""
  echo "##############################################################################################"
  echo ""
  sleep 5
done

