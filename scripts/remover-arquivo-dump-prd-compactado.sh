#!/usr/bin/env bash
cat >&1 <<-'EOWARN'
				**************************************************************
				INFO: Remover arquivo de dump e roles de producao
				**************************************************************
			EOWARN
nome_arquivo=$(ls /dump/*-prd.tar.gz | head -1)
if [ -e ${nome_arquivo} ]; then
    rm -vf /dump/*-prd.tar.gz
fi