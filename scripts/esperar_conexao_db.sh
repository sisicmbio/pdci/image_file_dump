#!/usr/bin/env bash

PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
PDCI_DB_USER=${PDCI_DB_USER:-postgres}
PDCI_DB_PASSWORD=${PDCI_DB_PASSWORD:-postgres}
PDCI_LIMIT=${PDCI_LIMIT:-300}
limit=${PDCI_LIMIT}
cat >&1 <<-'EOWARN'
				****************************************************
				INFO: Esperando conexao com o BD
				****************************************************
			EOWARN
#esperar conexao com o banco de dados
psql_on=$(PGPASSWORD=${PDCI_DB_PASSWORD}  psql -U  ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT}  -l 2>/dev/null | grep "List of databases")
echo "psql_on => ${psql_on}"
echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
echo "PDCI_DB_PORT => ${PDCI_DB_PORT}"
echo "PDCI_DB_USER => ${PDCI_DB_USER}"
echo "PDCI_DB_PASSWORD => ${PDCI_DB_PASSWORD}"
count=0
while [ -z "${psql_on}" ]; do
  if [ ${count} -lt ${limit} ]; then
    echo "****************************************************"
    echo " Esperando conexao com o banco (${PDCI_DB_HOST} : ${PDCI_DB_PORT} usuário ${PDCI_DB_USER} ) em   ${count}/${limit} segundos"
    echo "****************************************************"
    echo ""
    let count=count+1
    sleep 1
    psql_on=$(PGPASSWORD=${PDCI_DB_PASSWORD}  psql -U  ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT}  -l 2>/dev/null | grep "List of databases")
    if [ -z "${psql_on}" ]; then
     echo "..."
    else
     echo ""
     echo "Conexao estabelecida com o banco de dados."
     echo ""
    fi
  else
    psql_on="ESGOTOU O TEMPO DE ESPERA"
    echo "Tempo esgotado de esperar pela conexao ao banco de dados"
  fi
done
#FIM #esperar conexao com o banco de dados