#!/usr/bin/env bash

PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
PDCI_DB_NAME=db_dev_cotec
PDCI_ARQUIVO_DUMP_CUSTOMIZADO="/dump/db-customizado.sql"

esperar_conexao_db.sh

if [ -e "${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}" ] ; then
    echo
    echo "Restaurando banco customizado :: ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}"
    echo

    cmd="psql -U postgres -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} ${PDCI_DB_NAME} < ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}"
    echo "cmd :: ${cmd}"
    ret_cmd=$(${cmd});

    echo
    echo "Restauração customizada finalizada"
    echo
else
    echo
    echo "Sem arquivo customizado para restaurar :: ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}"
    echo
fi
