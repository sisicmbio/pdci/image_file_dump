#!/usr/bin/env sh
PDCI_DB_HOST=${PDCI_DB_HOST:-db_full_prd}
PDCI_DB_PORT=${PDCI_DB_PORT:-5432}
PDCI_DB_NAME=${PDCI_DB_NAME:-db_dev_cotec}
PDCI_DB_USER=${PDCI_DB_USER:-postgres}
PDCI_DB_PASSWORD=${PDCI_DB_PASSWORD:-postgres}

cat >&1 <<-'EOWARN'
				****************************************************
				INFO: Processo de importacao dos arquivos no diretorio /dump/:
				****************************************************
			EOWARN
pwd
#ls -la
ls -lah /dump/

for f in /dump/*; do
    echo "f => $f"
    case "$f" in
        *.sh)
            # https://github.com/docker-library/postgres/issues/450#issuecomment-393167936
            # https://github.com/docker-library/postgres/pull/452
            if [ -x "$f" ]; then
                echo "$0: running $f"
                "$f"
            else
                echo "$0: sourcing $f"
                . "$f"
            fi
            ;;
        *005-users.sql)
            echo "$0: running $f";
            echo
            PGPASSWORD=${PDCI_DB_PASSWORD}   psql -U ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${POSTGRES_DB:-postgres} < $f
        ;;
        *.sql)
            echo "$0: running $f";
            echo
            PGPASSWORD=${PDCI_DB_PASSWORD}   psql -U ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d ${PDCI_DB_NAME} < $f
        ;;
        *.dump)
            echo "$0: running $f";
            echo
             < $f
        ;;
        *db_dev_sigeo*.sql.gz)
            echo "$0: running $f";
            echo ""
            echo "#################################################################"
            echo " Restaurando dump em db_dev_sigeo ."
            echo "#################################################################"
            echo ""
            gunzip -c "$f" | PGPASSWORD=${PDCI_DB_PASSWORD}   psql -U ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} -d db_dev_sigeo ;
            echo
        ;;
        *db-customizado.sql.gz)
            echo "$0: running $f";
             echo ""
            echo "#################################################################"
            echo " Restaurando dump em db_dev_cotec ."
            echo "#################################################################"
            echo ""
            gunzip -c "$f" | PGPASSWORD=${PDCI_DB_PASSWORD}   psql -U ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} ;
            echo
        ;;
        *.gz)
            echo "$0: running $f";
            gunzip -c "$f" | PGPASSWORD=${PDCI_DB_PASSWORD}   psql -U ${PDCI_DB_USER} -w -h ${PDCI_DB_HOST} -p ${PDCI_DB_PORT} ;
            echo
        ;;
        *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

cat >&1 <<-'EOWARN'
				******************************************************************
				     INFO: Fim da importacao dos arquivos no diretorio /dump/:
				******************************************************************
			EOWARN