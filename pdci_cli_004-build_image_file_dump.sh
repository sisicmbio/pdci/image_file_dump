#!/usr/bin/env bash

echo "Numero de argumentos: $#"

if [ $# -lt 1 ]; then
   echo "Faltou passar  o caminho do registry como parametros"
   exit 1
fi

nome_imagem_file_dump="image_file_dump"
path_registry="$1/${nome_imagem_file_dump}:latest"
file=$(pwd)/dump/db-customizado.sql.gz

echo ""
echo ""
echo "nome_imagem_file_dump=${nome_imagem_file_dump}"

echo ""
echo ""
echo "path_registry=${path_registry}"

echo ""
echo ""
echo "file=${file}"



if [ -f "$file"  ]; then
    echo ""
    echo "Construindo imagem_file_dump com o arquivo $file"
    echo ""
    docker container prune -f
    echo ""
    docker system prune -f
    echo ""
    echo "Registry a ser disponibilizada a imagem_file_dump do projeto:: $2"
    echo ""
    docker build -f Dockerfile -t ${path_registry} . --no-cache
    echo ""
    docker push ${path_registry}
    echo ""
else
    echo ""
    echo "***********************************************************"
    echo "Arquivo /dump/db-customizado.sql.gz nao localizado.        "
    echo "Operacao Cancelada                                         "
    echo "***********************************************************"
    echo ""
fi
#
#
#ID=$(docker ps -a | grep registry.gitlab.com/pdci/postgis:prd-latest | grep db_postgis_prd | awk '{print $1}')
#echo "ID => ${ID}"
#cmd="docker commit ${ID}  registry.gitlab.com/pdci/app_laravel/postgis-dev:latest"
#
#docker commit ${ID}  registry.gitlab.com/pdci/app_laravel/postgis-dev:latest
#
##exec_cmd=$(${cmd})
##docker commit $ID  registry.gitlab.com/pdci/postgis-PRD:latest
#docker push registry.gitlab.com/pdci/app_laravel/postgis-dev:latest
##docker kill $ID
##docker rm $ID
###docker container prune -f
###Restaurar  o arquivo de produção em uma container do docker exemplo-pdci-004-customizar-bd-app_infocon