DO
$do$
BEGIN

  IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'usr_jenkins')
  THEN

    CREATE ROLE usr_jenkins LOGIN PASSWORD 'usr_jenkins'
      CREATEROLE
      VALID UNTIL 'infinity';
    COMMENT ON ROLE usr_jenkins
    IS 'Usuário que o servidor jenkins utiliza para se conectar com banco de dados.
Registra todas suas ações em jenkins.controle_versao.';

  END IF;

  IF NOT EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao'
  )
  THEN

    CREATE SCHEMA IF NOT EXISTS jenkins AUTHORIZATION usr_jenkins;
    COMMENT ON SCHEMA jenkins IS 'Tabela responsavel em controlar as atualizações de sql no banco de dados do ambiente';
    CREATE TABLE jenkins.controle_versao (des_tag text NOT NULL,  script_rodado text NOT NULL,data_inclusao text NOT NULL DEFAULT now(),ordem serial NOT NULL,CONSTRAINT _controle_versao_pk PRIMARY KEY (des_tag, script_rodado)) WITH (OIDS=FALSE);
--    ALTER TABLE jenkins.controle_versao OWNER TO usr_jenkins;
    COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado';

    CREATE INDEX controle_versao_pk_i ON jenkins.controle_versao USING btree (des_tag COLLATE pg_catalog."default", script_rodado COLLATE pg_catalog."default");

    ALTER ROLE usr_jenkins SET search_path TO jenkins;

    grant select,insert,update on all tables in schema jenkins to usr_jenkins;

    grant usage on all sequences in schema jenkins to usr_jenkins;

    grant execute on all functions in schema jenkins to usr_jenkins;

  END IF;

  -- Verifica em que banco esta rodando o script e concede permissão de crear schema ao usuario jenkins

  IF EXISTS (
      SELECT  -- SELECT list can stay empty for this
      FROM pg_database WHERE datistemplate = false AND datname = 'db_dev_cotec'
  )
  THEN

    GRANT CREATE ON DATABASE db_dev_cotec TO usr_jenkins;

  END IF;

  IF EXISTS (
      SELECT  -- SELECT list can stay empty for this
      FROM pg_database WHERE datistemplate = false AND datname = 'db_sisicmbio'
  )
  THEN

    GRANT CREATE ON DATABASE db_sisicmbio TO usr_jenkins;

  END IF;

  IF EXISTS (
      SELECT  -- SELECT list can stay empty for this
      FROM pg_database WHERE datistemplate = false AND datname = 'db_tcti_sisicmbio'
  )
  THEN

    GRANT CREATE ON DATABASE db_tcti_sisicmbio TO usr_jenkins;
  END IF;


  -- Verifica se o schema existe e caso exista concede a permissão ao usuario jenkins;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='alerta'
  )
  THEN

    GRANT ALL ON SCHEMA alerta TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema alerta to usr_jenkins;

    grant usage on all sequences in schema alerta to usr_jenkins;

    grant execute on all functions in schema alerta to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='asiweb'
  )
  THEN

    GRANT ALL ON SCHEMA asiweb TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema asiweb to usr_jenkins;

    grant usage on all sequences in schema asiweb to usr_jenkins;

    grant execute on all functions in schema asiweb to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='auditoria'
  )
  THEN

    GRANT ALL ON SCHEMA auditoria TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema auditoria to usr_jenkins;

    grant usage on all sequences in schema auditoria to usr_jenkins;

    grant execute on all functions in schema auditoria to usr_jenkins;

  END IF;
  IF EXISTS(
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='brigadista'
  )
  THEN

    GRANT ALL ON SCHEMA brigadista TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema brigadista to usr_jenkins;

    grant usage on all sequences in schema brigadista to usr_jenkins;

    grant execute on all functions in schema brigadista to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='canie'
  )
  THEN

    GRANT ALL ON SCHEMA canie TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema canie to usr_jenkins;

    grant usage on all sequences in schema canie to usr_jenkins;

    grant execute on all functions in schema canie to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='capacitacao'
  )
  THEN

    GRANT ALL ON SCHEMA capacitacao TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema capacitacao to usr_jenkins;

    grant usage on all sequences in schema capacitacao to usr_jenkins;

    grant execute on all functions in schema capacitacao to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='cis'
  )
  THEN

    GRANT ALL ON SCHEMA cis TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema cis to usr_jenkins;

    grant usage on all sequences in schema cis to usr_jenkins;

    grant execute on all functions in schema cis to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='corporativo'
  )
  THEN

    GRANT ALL ON SCHEMA corporativo TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema corporativo to usr_jenkins;

    grant usage on all sequences in schema corporativo to usr_jenkins;

    grant execute on all functions in schema corporativo to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='especies'
  )
  THEN

    GRANT ALL ON SCHEMA especies TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema especies to usr_jenkins;

    grant usage on all sequences in schema especies to usr_jenkins;

    grant execute on all functions in schema especies to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='geo'
  )
  THEN

    GRANT ALL ON SCHEMA geo TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema geo to usr_jenkins;

    grant usage on all sequences in schema geo to usr_jenkins;

    grant execute on all functions in schema geo to usr_jenkins;

  END IF;

  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='public'
  )
  THEN

    GRANT ALL ON SCHEMA public TO usr_jenkins WITH GRANT OPTION;

    grant select  on all tables in schema public to usr_jenkins;

    grant execute on all functions in schema public to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='salve'
  )
  THEN

    GRANT ALL ON SCHEMA salve TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema salve to usr_jenkins;

    grant usage on all sequences in schema salve to usr_jenkins;

    grant execute on all functions in schema salve to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sarr'
  )
  THEN

    GRANT ALL ON SCHEMA sarr TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sarr to usr_jenkins;

    grant usage on all sequences in schema sarr to usr_jenkins;

    grant execute on all functions in schema sarr to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sct'
  )
  THEN

    GRANT ALL ON SCHEMA sct TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sct to usr_jenkins;

    grant usage on all sequences in schema sct to usr_jenkins;

    grant execute on all functions in schema sct to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sgca'
  )
  THEN

    GRANT ALL ON SCHEMA sgca TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sgca to usr_jenkins;

    grant usage on all sequences in schema sgca to usr_jenkins;

    grant execute on all functions in schema sgca to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sgd'
  )
  THEN

    GRANT ALL ON SCHEMA sgd TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sgd to usr_jenkins;

    grant usage on all sequences in schema sgd to usr_jenkins;

    grant execute on all functions in schema sgd to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sgebio'
  )
  THEN

    GRANT ALL ON SCHEMA sgebio TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sgebio to usr_jenkins;

    grant usage on all sequences in schema sgebio to usr_jenkins;

    grant execute on all functions in schema sgebio to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sica'
  )
  THEN

    GRANT ALL ON SCHEMA sica TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sica to usr_jenkins;

    grant usage on all sequences in schema sica to usr_jenkins;

    grant execute on all functions in schema sica to usr_jenkins;


  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sicae'
  )
  THEN

    GRANT ALL ON SCHEMA sicae TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sicae to usr_jenkins;

    grant usage on all sequences in schema sicae to usr_jenkins;

    grant execute on all functions in schema sicae to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sige'
  )
  THEN

    GRANT ALL ON SCHEMA sige TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sige to usr_jenkins;

    grant usage on all sequences in schema sige to usr_jenkins;

    grant execute on all functions in schema sige to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sigterra'
  )
  THEN

    GRANT ALL ON SCHEMA sigterra TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sigterra to usr_jenkins;

    grant usage on all sequences in schema sigterra to usr_jenkins;

    grant execute on all functions in schema sigterra to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='simac'
  )
  THEN

    GRANT ALL ON SCHEMA simac TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema simac to usr_jenkins;

    grant usage on all sequences in schema simac to usr_jenkins;

    grant execute on all functions in schema simac to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sisbio'
  )
  THEN

    GRANT ALL ON SCHEMA sisbio TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sisbio to usr_jenkins;

    grant usage on all sequences in schema sisbio to usr_jenkins;

    grant execute on all functions in schema sisbio to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sisbv'
  )
  THEN

    GRANT ALL ON SCHEMA sisbv TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sisbv to usr_jenkins;

    grant usage on all sequences in schema sisbv to usr_jenkins;

    grant execute on all functions in schema sisbv to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='siscorc'
  )
  THEN

    GRANT ALL ON SCHEMA siscorc TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema siscorc to usr_jenkins;

    grant usage on all sequences in schema siscorc to usr_jenkins;

    grant execute on all functions in schema siscorc to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sisfamilias'
  )
  THEN

    GRANT ALL ON SCHEMA sisfamilias TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sisfamilias to usr_jenkins;

    grant usage on all sequences in schema sisfamilias to usr_jenkins;

    grant execute on all functions in schema sisfamilias to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sismidia'
  )
  THEN

    GRANT ALL ON SCHEMA sismidia TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sismidia to usr_jenkins;

    grant usage on all sequences in schema sismidia to usr_jenkins;

    grant execute on all functions in schema sismidia to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sisva'
  )
  THEN

    GRANT ALL ON SCHEMA sisva TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sisva to usr_jenkins;

    grant usage on all sequences in schema sisva to usr_jenkins;

    grant execute on all functions in schema sisva to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sit'
  )
  THEN

    GRANT ALL ON SCHEMA sit TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sit to usr_jenkins;

    grant usage on all sequences in schema sit to usr_jenkins;

    grant execute on all functions in schema sit to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='sofia'
  )
  THEN

    GRANT ALL ON SCHEMA sofia TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema sofia to usr_jenkins;

    grant usage on all sequences in schema sofia to usr_jenkins;

    grant execute on all functions in schema sofia to usr_jenkins;

  END IF;
  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='taxonomia'
  )
  THEN

    GRANT ALL ON SCHEMA taxonomia TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema taxonomia to usr_jenkins;

    grant usage on all sequences in schema taxonomia to usr_jenkins;

    grant execute on all functions in schema taxonomia to usr_jenkins;

  END IF;

  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='tiger'
  )
  THEN

    GRANT ALL ON SCHEMA tiger TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema tiger to usr_jenkins;

    grant usage on all sequences in schema tiger to usr_jenkins;

    grant execute on all functions in schema tiger to usr_jenkins;

  END IF;

  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='tiger_data'
  )
  THEN

    GRANT ALL ON SCHEMA tiger_data TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema tiger_data to usr_jenkins;

    grant usage on all sequences in schema tiger_data to usr_jenkins;

    grant execute on all functions in schema tiger_data to usr_jenkins;

  END IF;


  IF NOT EXISTS (
      SELECT  -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'usr_monitora')
  THEN

    CREATE ROLE usr_monitora LOGIN PASSWORD 'usr_monitora'
    CREATEROLE
      VALID UNTIL 'infinity';
    COMMENT ON ROLE usr_monitora
    IS 'Usuário que a aplicação com o mesmo nome  se conecta com o schema do banco de dados.';

    CREATE SCHEMA IF NOT EXISTS monitora AUTHORIZATION usr_jenkins;


  END IF;

  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='monitora'
  )
  THEN

    GRANT ALL ON SCHEMA monitora TO usr_jenkins WITH GRANT OPTION;

    grant select,insert,update on all tables in schema monitora to usr_jenkins;
    grant usage on all sequences in schema monitora to usr_jenkins;
    grant execute on all functions in schema monitora to usr_jenkins;


    grant select,insert,update on all tables in schema monitora to usr_monitora;
    grant usage on all sequences in schema monitora to usr_monitora;
    grant execute on all functions in schema monitora to usr_monitora;

  END IF;



  IF NOT EXISTS (
      SELECT  -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'usr_lafsisbio')
  THEN

    CREATE ROLE usr_lafsisbio LOGIN PASSWORD 'usr_lafsisbio'
      CREATEROLE
      VALID UNTIL 'infinity';
    COMMENT ON ROLE usr_lafsisbio
    IS 'Usuário que a aplicação com o mesmo nome  se conecta com o schema do banco de dados.';

    CREATE SCHEMA IF NOT EXISTS lafsisbio AUTHORIZATION usr_jenkins;

  END IF;

  IF EXISTS (
      SELECT -- SELECT list can stay empty for this
      FROM information_schema.tables  WHERE table_schema='lafsisbio'
  )
  THEN

    GRANT ALL ON SCHEMA lafsisbio TO usr_jenkins WITH GRANT OPTION;


    grant select,insert,update on all tables in schema lafsisbio to usr_jenkins;
    grant usage on all sequences in schema lafsisbio to usr_jenkins;
    grant execute on all functions in schema lafsisbio to usr_jenkins;


    grant select,insert,update on all tables in schema lafsisbio to usr_lafsisbio;
    grant usage on all sequences in schema lafsisbio to usr_lafsisbio;
    grant execute on all functions in schema lafsisbio to usr_lafsisbio;

  END IF;


END
$do$;